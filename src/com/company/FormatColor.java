package com.company;

public class FormatColor {
    private String color="";
    private String background="";
    private boolean highIntensity=false;
    private boolean boldHighIntensity=false;
    private boolean bold=false;
    private boolean underline=false;
    public void setColor(String color) {
        this.color = color;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public void setBold(boolean bold) {
        clearFont();
        this.bold = bold;
    }

    public void setUnderline(boolean underline) {
        clearFont();
        this.underline = underline;
    }

    public void setHighIntensity(boolean highIntensity) {
        clearFont();
        this.highIntensity = highIntensity;
    }

    public void setBoldHighIntensity(boolean boldHighIntensity) {
        clearFont();
        this.boldHighIntensity = boldHighIntensity;
    }

    public String getText(String text){
        if(bold==true){
            color=color.replace("[0","[1");
        }
        if(underline==true){
            color=color.replace("[0","[4");
        }

        if(boldHighIntensity==true){
            color=color.replace("[0","[1");
            color=color.replace(";3",";9");
        }
        if(highIntensity==true){
            color=color.replace(";3",";9");
        }
        return color + background + text;
    }
    private void clearFont(){
        bold=false;
        underline=false;
        boldHighIntensity=false;
        highIntensity=false;
    }
}
