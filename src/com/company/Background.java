package com.company;

public class Background {
    // Background
    public static final String BLACK = "\033[40m";  // BLACK
    public static final String RED = "\033[41m";    // RED
    public static final String GREEN = "\033[42m";  // GREEN
    public static final String YELLOW = "\033[43m"; // YELLOW
    public static final String BLUE = "\033[44m";   // BLUE
    public static final String PURPLE = "\033[45m"; // PURPLE
    public static final String CYAN = "\033[46m";   // CYAN
    public static final String WHITE = "\033[47m";  // WHITE
}
